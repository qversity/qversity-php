<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class Emailer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $template;
    protected $data;
    protected $to;
    protected $cc;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->to = (isset($data['to']))?$data['to']:[];
        $this->cc = (isset($data['cc']))?$data['cc']:[];
        $this->template = (isset($data['template']))?trim($data['template']):'';
        $this->data = (isset($data['data']))?$data['data']:[];
        $this->subject = (isset($data['subject']))?trim($data['subject']):'';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        if(!empty($this->template) && !empty($this->to)) {
            Mail::send($this->template, $this->data, function ($message) {
                foreach ($this->to as $row) {
                    $message->to($row['email'], $row['name']);
                }
                if(!empty($this->cc)) {
                    foreach ($this->cc as $row) {
                        $message->to($row['email'], $row['name']);
                    }
                }
                $message->subject($this->subject);
            });
        }
    }
}
