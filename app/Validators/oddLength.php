<?php
namespace App\Validators;

class oddLength{
    public function validate($attribute, $value, $parameters, $validator){
        if(strlen($value)%2 ==0){
            return false;
        }else{
            return true;
        }
    }
}
