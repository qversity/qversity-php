<?php

if (! function_exists('encryptId')) {
    function encryptId($string) {
        $output = false;
        $encrypt_method = env('ENCRYPTION_METHOD');
        $secret_key = env('ENCRYPTION_KEY');
        $secret_iv = env('ENCRYPTION_IV');
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }
}

if (! function_exists('decryptId')) {
    function decryptId($string) {
        $output = false;
        $encrypt_method = env('ENCRYPTION_METHOD');
        $secret_key = env('ENCRYPTION_KEY');
        $secret_iv = env('ENCRYPTION_IV');
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}
