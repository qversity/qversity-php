<?php

namespace App\Providers\Admin;

use Illuminate\Support\ServiceProvider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use DB;
use Laracasts\Flash\Flash;
use App\Jobs\Emailer;

class UserServiceProvider extends ServiceProvider
{


public static function register($request){
	
	
	echo $date = date('Y-m-d H:i:s');//exit;
     
        $email = $request['email'];
        $first_name = $request['name'];
        $password = bcrypt($request['password']);

        $user = new User();
        $user->email = $email;
        $user->name = $first_name;
        $user->password = $password;
        $user->role = User::ROLE_STUDENT;
        $user->status = User::STATUS_ACTIVE;
        $user->last_login = $date;
        $user->linkedin_id = '';
		$user->googleplusId = '';
		$user->signup_form = '';
		
        $user->save();
        Auth::login($user);
        if($user->id){
            return true;
        }else{
            return false;
        }
        
    }

    public static function createUser($input){
        $password  = str_random(8);
        $user = new User();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt($password);
        $user->status = User::STATUS_ACTIVE;
        $user->role = $input['role'];
        $user->lastLogin = date('Y-m-d H:i:s');
        $user->save();
        if($user->id){
            $data = array(
                'template' =>'emails.admin.user_credential',
                'subject'=> trans('messages.common.welcome_onboard'),
                'to'=> array(array('name'=>$user->name,'email'=>$user->email)),
                //'cc' => array(array('name'=>'abhishek','email'=>'abhishek.patel@qversity.com')),
                'data'=> array('userId'=>$user->email,'password'=>$password)
            );
            dispatch(new Emailer($data));
            return true;
        }else{
            return false;
        }
    }
}
