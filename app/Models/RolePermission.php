<?php

namespace App\Models;
use App\Models\Role;
use App\Models\Permission;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends BaseModel
{
   protected $table = 'role_permission';
    protected $appends = ['encrypted_id'];
   
   public function role(){
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
    
    public function permission(){
        return $this->belongsTo(Permission::class, 'permission_id', 'id');
    }
}
