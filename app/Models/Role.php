<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Permission;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Role extends BaseModel
{
    use SoftDeletes;

    protected $appends = ['encrypted_id'];
    
}
