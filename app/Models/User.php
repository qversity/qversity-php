<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use SoftDeletes;

    protected $appends = ['role_name','encrypted_id'];

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_ADMIN = 2;
    const ROLE_PRIMARY_UNIVERSITY = 3;
    const ROLE_SECONDARY_UNIVERISTY = 4;
    const ROLE_INSTRUCTOR = 5;
    const ROLE_STUDENT = 6;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function getAttribute($key){
        if (array_key_exists($key, $this->relations)|| method_exists($this, $key)) {
            return parent::getAttribute($key);
        }
        else {
            return parent::getAttribute(snake_case($key));
        }
    }

    public function setAttribute($key, $value){
        return parent::setAttribute(snake_case($key), $value);
    }

    public function getEncryptedIdAttribute(){
        return encryptId($this->id);
    }

    public function userRole(){
        return $this->belongsTo(Role::class,'role','id');
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute(){
        switch($this->role){
            case self::ROLE_SUPER_ADMIN:
                return "Super Admin";
                break;
            case self::ROLE_ADMIN:
                return "Admin";
                break;
            case self::ROLE_PRIMARY_UNIVERSITY:
                return "Primary University";
                break;
            case self::ROLE_SECONDARY_UNIVERISTY:
                return "Secondary University";
                break;
            case self::ROLE_INSTRUCTOR:
                return "Instructor";
                break;
            case self::ROLE_STUDENT:
                return "Student";
                break;
             default:
                return "NA";
        }
    }

}
