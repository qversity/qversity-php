<?php

namespace App\Models;
use App\Models\Role;
use Faker\Provider\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\Model;

class Permission extends BaseModel
{
    
     use SoftDeletes;

    protected $appends = ['encrypted_id'];
}
