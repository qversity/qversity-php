<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\Admin\UserServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  //  protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
	protected $img = '';
    
	public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
                
    }
	
    public function handleProviderCallback($social)
    {
         
    $userSocial = Socialite::driver($social)->user();
    $date = date('Y-m-d H:i:s');
    $firstName = $userSocial->getName();
    $email = $userSocial->getEmail();
    $socialId = $userSocial->id;
    
	$this->img = $userSocial->avatar_original;
        //$avatarUrl = $this->graphUrl.'/'.$this->version.'/'.$user['id'].'/picture';
        if($social == 'facebook'){
            $userSocialId = User::wherefacebookId($socialId)->first();   
        }elseif($social == 'google'){
            $userSocialId = User::wheregoogleplusId($socialId)->first();
        }
        $user = User::whereEmail($email)->first();
        if ($userSocialId) {
            $user->lastLogin = $date;   
            $user->save();
            Auth::loginUsingId($user->id);
            return redirect(route('student.dashboard'));
        }elseif ($user) {
            if($social == 'facebook'){
                $user->facebookId = $socialId;
            }elseif($social == 'google'){
                $user->googleplusId = $socialId;
            }
            $user->lastLogin = $date;   
            $user->save();
            Auth::loginUsingId($user->id);
            return redirect(route('student.dashboard'));
        }else {
            $user = new User();
            $user->email = $email;
            $user->name = $firstName;
            $user->role = User::ROLE_STUDENT;
            $user->status = User::STATUS_ACTIVE;
            $user->image = $this->img;
            if($social == 'facebook'){
                $user->facebookId = $socialId;
            }elseif($social == 'google'){
                $user->googleplusId = $socialId;
            }
            $user->lastLogin = $date;		  	
            $user->save();
            Auth::loginUsingId($user->id);
            return redirect(route('student.dashboard'));
        }
    }	
}
