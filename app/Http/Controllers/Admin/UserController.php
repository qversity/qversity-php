<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Providers\Admin\UserServiceProvider;
use Laracasts\Flash\Flash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $results = User::all();

            return DataTables::of($results)
                ->editColumn('status', function($result){
                    $status = 'Inactive';
                    if($result->status == User::STATUS_ACTIVE) {
                        $status = 'Active';
                    }
                    return $status;
                })
                ->addColumn('action', 'admin.users.datatables_actions')
                ->make(TRUE);
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request){
        $res = UserServiceProvider::createUser($request->all());
        if($res){
            Flash::success('User created successfully');
            return redirect(route('user.index'));
        }else{
            Flash::error('Something went wrong. Please try again later.');
            return redirect(route('user.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
       echo decryptId($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $userId = decryptId($id);
        $user = User::find($userId);
        if(empty($user)){
            Flash::error('User not available');
            return redirect(route('user.index'));
        }
        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = decryptId($id);
        $user = User::find($userId);
        if(empty($user)){
            Flash::error('User not available');
            return redirect(route('user.index'));
        }
        $user->name = $request->name;
        $user->role = $request->role;
        $res = $user->save();
        if($res){
            Flash::success('User updated successfully');
        }else{
            Flash::error('Oops! Something went wrong. Please try again later.');
        }
        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = decryptId($id);
        $user = User::find($userId);
        if(empty($user)){
            Flash::error('User not available');
            return redirect(route('user.index'));
        }
        $res = $user->delete();
        if($res){
            Flash::success('User deleted successfully');
        }else{
            Flash::error('Oops! Something went wrong. Please try again later.');
        }
        return redirect(route('user.index'));
    }

    public function toggleStatus($id){
        $userId = decryptId($id);
        $user = User::find($userId);
        if(empty($user)){
            Flash::error('User not available');
            return redirect(route('user.index'));
        }
        if($user->status == User::STATUS_ACTIVE){
            $user->status = User::STATUS_INACTIVE;
        }else{
            $user->status = User::STATUS_INACTIVE;
        }
        $res = $user->save();
        if($res){
            Flash::success('User\'s status updated successfully');
        }else{
            Flash::error('Oops! Something went wrong. Please try again later.');
        }
        return redirect(route('user.index'));
    }
}
