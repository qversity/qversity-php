<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use DB;
use App\Http\Requests\Admin\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('adminhome');
    }

    public function loginView(Request $request){
        if(Auth::user()){
            return redirect(route('admin.dashboard'));
        }
        return view('Auth/login');
    }

    public function login(LoginRequest $request){
        $user  = User::whereEmail($request->email)->whereIn('role',array(User::ROLE_SUPER_ADMIN,User::ROLE_ADMIN))->whereStatus(User::STATUS_ACTIVE)->first();
        if(!empty($user)){
            if(Hash::check($request->password, $user->password)){
                Auth::login($user);
                return redirect(route('admin.dashboard'));
            }
        }
        return redirect()->back()->with(['message' => trans('messages.admin.invalid_credential')]);
    }

    public function logout(){
        Auth::logout();
        return redirect(route('admin.login'));
    }

    public function dashboard(){
        return view('admin.dashboard');
    }


}
