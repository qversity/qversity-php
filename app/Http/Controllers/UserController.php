<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\Admin\UserServiceProvider;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\RegisterRequest;
use App\Models\User;
use DB;


class UserController extends Controller
{
    private $date ;
    public function signUp(RegisterRequest $request){
		UserServiceProvider::register($request->all());
		return redirect(route('admin.dashboard'));      

    }
    
    public function logOut(){
        Auth::logout();
		return redirect(url('/'));

    }
	
    public function signIn(LoginRequest $request)
    {
        $this->date = date('Y-m-d H:i:s');
        if (Auth::attempt(['email' => $request['email'],'password' => $request['password'],'status'=>User::STATUS_ACTIVE])) {
            $user = User::where('email', $request['email'])->first();
            $user->lastLogin = $this->date;
            $user->save();
                    
            return redirect(route('admin.dashboard'));
        }
        return redirect()->back()->with(['message' => trans('messages.common.invalid_credential')]);
    }
	
}
