<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class AdminAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if (!empty(\Auth::User()) && \Auth::User()->role == User::ROLE_SUPER_ADMIN) {
            return $next($request);
        }
        return redirect(route('admin.login'));
    }
}
