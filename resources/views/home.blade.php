@extends('layouts.master')
@section('content')
<div class="banner">
  <img src="{{ URL::to('images/banner.jpg') }}" class="img-responsive">
  <div class="d_form"> <a class="talk_btn" >TALK TO AN EXPERT</a>
    <div class="talk_form">
      <h2>TALK TO AN EXPERT</h2>
      <div class="close_btn" id="close_b"><span><i class="fa fa-close"></i></span></div>
      <form>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="E-Mail">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Mobile">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <button type="submit" class="btn_qry btn-effetc">Request to call</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="m_form"> <a class="mobile_chat" data-toggle = "modal" data-target = "#mobile_chat"><i class="fa fa-comments-o"></i></a> 
    <!-- Login Popup -->
    <div class = "modal fade" id = "mobile_chat" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
      <div class = "modal-dialog ">
        <div class = "modal-content mobile_talk">
          <div class = "modal-body">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true"> &times; </button>
            <h2>TALK TO AN DESIGNER</h2>
            <form>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="E-Mail">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Mobile">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <button type="submit" class="btn_qry btn-effetc">Request to call</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="why_choose">
  <div class="container">
    <h1>Why Choose QVersity</h1>
    <div class="row">
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap"> <div class="choose_list choose1">
        <div class="c_icon"><img src="{{ URL::to('images/choose1.png') }}" alt=""></div>
        <h2>Live Instructor Session</h2>
        </div> </div>
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap"> <div class="choose_list choose2">
        <div class="c_icon"><img src="{{ URL::to('images/choose2.png') }}" alt=""></div>
        <h2>US University Certification</h2>
        </div> </div>
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap"> <div class="choose_list choose3">
        <div class="c_icon"><img src="{{ URL::to('images/choose3.png') }}" alt=""></div>
        <h2>Personalized Learning</h2>
        </div> </div>
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap"> <div class="choose_list choose4">
        <div class="c_icon"><img src="{{ URL::to('images/choose4.png') }}" alt=""></div>
        <h2>In-Demand Courses</h2>
        </div> </div>
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap"> <div class="choose_list choose5">
        <div class="c_icon"><img src="{{ URL::to('images/choose5.png') }}" alt=""></div>
        <h2>Interactive Classes</h2>
        </div> </div>
      <div class="col-xs-6 col-sm-4 col-md-2 choose_gap "> <div class="choose_list choose6">
        <div class="c_icon"><img src="{{ URL::to('images/choose6.png') }}" alt=""></div>
        <h2>Experienced Practitioners</h2>
        </div> </div>
    </div>
  </div>
</section>
<section class="about_us">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 about_content">
        <h2>Welcome To QVersity</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. More-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English</p>
        <a href="#" class="btn-learn">Learn More</a> </div>
      <div class="col-xs-12 col-sm-12 col-md-6 about_image"> <img src="images/about.png" class="img-responsive" alt=""> </div>
    </div>
  </div>
</section>
<section class="popular_course">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 sub_title">
      <h2>Popular Courses</h2>
      <div class="line"><img src="{{ URL::to('images/title-icon.png') }}" alt=""></div>
      <p>Lorem Ipsum is simply dummy text of the printing</p>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="course_list">
          <div class="course_image"> <img src="{{ URL::to('images/course1.png') }}" class="img-responsive" alt=""> </div>
          <div class="rating_box">
            <div class="left_view">
              <ul>
                <li><a href="#"><span><i class="fa fa-eye"></i></span>59</a></li>
                <li><a href="#"><span><i class="fa fa-comments"></i></span>29</a></li>
              </ul>
            </div>
            <div class="right_star">
              <ul>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star_half.png') }}" alt=""></li>
              </ul>
            </div>
          </div>
          <div class="course_text">
            <h3>We Are Creative Designer</h3>
            <ul class="author-name">
              <li><span>by:</span>John Smith</li>
              <li><span>Date:</span>20.5.15</li>
            </ul>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="course_list">
          <div class="course_image"> <img src="{{ URL::to('images/course2.png') }}" class="img-responsive" alt=""> </div>
          <div class="rating_box">
            <div class="left_view">
              <ul>
                <li><a href="#"><span><i class="fa fa-eye"></i></span>59</a></li>
                <li><a href="#"><span><i class="fa fa-comments"></i></span>29</a></li>
              </ul>
            </div>
            <div class="right_star">
              <ul>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star_half.png') }}" alt=""></li>
              </ul>
            </div>
          </div>
          <div class="course_text">
            <h3>We Are Creative Designer</h3>
            <ul class="author-name">
              <li><span>by:</span>John Smith</li>
              <li><span>Date:</span>20.5.15</li>
            </ul>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="course_list">
          <div class="course_image"> <img src="{{ URL::to('images/course3.png') }}" class="img-responsive" alt=""> </div>
          <div class="rating_box">
            <div class="left_view">
              <ul>
                <li><a href="#"><span><i class="fa fa-eye"></i></span>59</a></li>
                <li><a href="#"><span><i class="fa fa-comments"></i></span>29</a></li>
              </ul>
            </div>
            <div class="right_star">
              <ul>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star.png') }}" alt=""></li>
                <li><img src="{{ URL::to('images/star_half.png') }}" alt=""></li>
              </ul>
            </div>
          </div>
          <div class="course_text">
            <h3>We Are Creative Designer</h3>
            <ul class="author-name">
              <li><span>by:</span>John Smith</li>
              <li><span>Date:</span>20.5.15</li>
            </ul>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
          </div>
        </div>
      </div>
      <div class="all_course"> <a href="#">Browse All Courses</a> </div>
    </div>
  </div>
</section>
<section class="our_activities">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="activity_list">
          <div class="act_icon"><img src="{{ URL::to('images/teacher.png') }}" alt=""></div>
          <div class="act_text">
            <h3>Teachers</h3>
            <p>34+</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="activity_list">
          <div class="act_icon"><img src="{{ URL::to('images/student.png') }}" alt=""></div>
          <div class="act_text">
            <h3>Students</h3>
            <p>3554+</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="activity_list">
          <div class="act_icon"><img src="{{ URL::to('images/university.png') }}" alt=""></div>
          <div class="act_text">
            <h3>University</h3>
            <p>357+</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="activity_list">
          <div class="act_icon"><img src="{{ URL::to('images/university.png') }}" alt=""></div>
          <div class="act_text">
            <h3>Award</h3>
            <p>44+</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="meet_lecturers">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 sub_title">
      <h2>Meet Our Lecturers</h2>
      <div class="line"><img src="images/title-icon2.png" alt=""></div>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard<br>
        dummy text ever since the 1500s, when an unknown printer took a galley.</p>
    </div>
    <div id="testimonial-slider" class="owl-carousel col-xs-12 col-sm-12 col-md-12">
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer1.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer2.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer3.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer1.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer2.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer3.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer1.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer2.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="lecturer_list">
          <div class="lec_image"><img src="images/lecturer3.png" class="img-responsive" alt=""></div>
          <div class="lec_text">
            <h3>John Smith</h3>
            <p>Finance Teacher</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="upcoming_event">
<div class="container">
<h2>Upcoming Education Events</h2>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="upevent_list">
      <div class="upevent_date">
        <h3>21</h3>
        <span>October 2017</span> </div>
      <div class="upevent_picture"><img src="images/event1.png" class="img-responsive" alt=""></div>
      <div class="upevent_text">
        <h4>The Time Value of Money</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
        <ul>
          <li><span><i class="fa fa-clock-o"></i></span>10:00 AM - 3:00 PM</li>
          <li><span><i class="fa fa-map-marker"></i></span>New York, US</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="upevent_list">
      <div class="upevent_date">
        <h3>09</h3>
        <span>October 2017</span> </div>
      <div class="upevent_picture"><img src="images/event1.png" class="img-responsive" alt=""></div>
      <div class="upevent_text">
        <h4>The Cost of Capital</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
        <ul>
          <li><span><i class="fa fa-clock-o"></i></span>10:00 AM - 3:00 PM</li>
          <li><span><i class="fa fa-map-marker"></i></span>New York, US</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12"> <a href="" class="btn_view">View all</a> </div>
</div>
</section>
<section class="blog">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 sub_title">
      <h2>From the Blog</h2>
      <div class="line"><img src="images/title-icon3.png" alt=""></div>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
    <div id="testimonial-slider1" class="owl-carousel col-xs-12 col-sm-12 col-md-12">
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="blog_list">
          <div class="blog_image"><img src="images/blog1.jpg" class="img-responsive" alt=""></div>
          <div class="blog_text">
            <ul>
              <li><span><i class="fa fa-calendar"></i></span>October 4, 2018</li>
              <li><span><i class="fa fa-user"></i></span>Arone Hamilton</li>
              <li><a href="#"><span><i class="fa fa-heart-o"></i></span>8</a></li>
              <li><a href="#"><span><i class="fa fa-comments"></i></span>10</a></li>
            </ul>
            <h3>Lorem Ipsum is simply dummy text</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
            <a href="#" class="btn_more">Read More...</a> </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="blog_list">
          <div class="blog_image"><img src="images/blog2.jpg" class="img-responsive" alt=""></div>
          <div class="blog_text">
            <ul>
              <li><span><i class="fa fa-calendar"></i></span>October 4, 2018</li>
              <li><span><i class="fa fa-user"></i></span>Arone Hamilton</li>
              <li><a href="#"><span><i class="fa fa-heart-o"></i></span>8</a></li>
              <li><a href="#"><span><i class="fa fa-comments"></i></span>10</a></li>
            </ul>
            <h3>Lorem Ipsum is simply dummy text</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
            <a href="#" class="btn_more">Read More...</a> </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 mob_gap">
        <div class="blog_list">
          <div class="blog_image"><img src="images/blog1.jpg" class="img-responsive" alt=""></div>
          <div class="blog_text">
            <ul>
              <li><span><i class="fa fa-calendar"></i></span>October 4, 2018</li>
              <li><span><i class="fa fa-user"></i></span>Arone Hamilton</li>
              <li><a href="#"><span><i class="fa fa-heart-o"></i></span>8</a></li>
              <li><a href="#"><span><i class="fa fa-comments"></i></span>10</a></li>
            </ul>
            <h3>Lorem Ipsum is simply dummy text</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
            <a href="#" class="btn_more">Read More...</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-2">
        <div class="footer_list">
          <h2>Company</h2>
          <ul>
            <li><a href="#">What We Do</a></li>
            <li><a href="#">How It Works</a></li>
            <li><a href="#">Teach for QV</a></li>
            <li><a href="#">Payment & Financing</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-2">
        <div class="footer_list">
          <h2>Quick Links</h2>
          <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Opening Hours</a></li>
            <li><a href="#">Our Top Courses</a></li>
            <li><a href="#">Why Choose QVersity</a></li>
            <li><a href="#">Upcoming Events</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-2">
        <div class="footer_list">
          <h2>Courses</h2>
          <ul>
            <li><a href="#">Financial Modeling</a></li>
            <li><a href="#">Busines Leader's Guide</a></li>
            <li><a href="#">Computer Science</a></li>
            <li><a href="#">Micro Biology</a></li>
            <li><a href="#">Cyber Security</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-6">
        <div class="footer_list">
          <h2>Contact Us</h2>
          <div class="row">
            <form class="footer_form">
              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Name*">
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Email*">
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <textarea class="form-control" rows="1" placeholder="Message*"></textarea>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <button class="btn_send">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<div class="footer_bottom">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="footer_address">
          <ul>
            <li><span><i class="fa fa-phone"></i></span>Phone +1 929.841.5969</li>
            <li><span><i class="fa fa-envelope"></i></span>info@qversity.com</li>
            <li><span><i class="fa fa-map-marker"></i></span>242 West 30th Street, Suite 900, New York, NY 10001</li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="footer_copyright">
          <p>&copy; QVERSITY INC. <span>ALL RIGHTS RESERVED.</span></p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Login Popup -->
<div class = "modal fade" id = "login" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
  <div class = "modal-dialog login_popup">
    <div class = "modal-content">
      <div class = "modal-header">
        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true"> &times; </button>
        <h2>Login to your QVersity account!</h2>
      </div>
	<form class="form-horizontal" role="form" method="POST" action="{{ url('/signin') }}">
	  @csrf
      <div class = "modal-body">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="social_btn">
            <ul>
              <li class="fa_login"><a href="{{ url('login/facebook') }}"><span><i class="fa fa-facebook"></i></span>Continue with Facebook</a></li>
              <li class="fa_google"><a href="{{ url('login/google') }}"><span><i class="fa fa-google-plus "></i></span>Continue with Google</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <div class = "input-group"> <span class = "input_addon"><i class="fa fa-envelope"></i></span>
                  <input type="text" name="email" class="form-control" placeholder="Email">
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <div class = "input-group"> <span class = "input_addon"><i class="fa fa-lock"></i></span>
                  <input type="text" name="password" class="form-control" placeholder="Password">
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
				<button type="submit" class="btn_submit">Sign in</button>
              </div>
            </div>
          </div>
          <div class="forgot_password">
            <p><a href="#">Forgot Password</a></p>
          </div>
          <div class="term_c">
            <p>By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy.</a></p>
          </div>
          <div class="sign_up">
            <p>Don't have an account? <a href="#">Sign up</a></p>
          </div>
        </div>
      </div>
	</form>
    </div>
  </div>
</div>

<!-- Signup Popup -->
<div class = "modal fade" id = "signup" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
  <div class = "modal-dialog login_popup">
    <div class = "modal-content">
      <div class = "modal-header">
        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true"> &times; </button>
        <h2>Sign Up and Start Learning!</h2>
      </div>
	  <form class="form-horizontal" role="form" method="POST" action="{{ url('/signup') }}">
	  {{ csrf_field() }}
      <div class = "modal-body">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <div class = "input-group"> <span class = "input_addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" name="name" placeholder="Full Name">
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <div class = "input-group"> <span class = "input_addon"><i class="fa fa-envelope"></i></span>
                  <input type="text" class="form-control" name="email" placeholder="Email">
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <div class = "input-group"> <span class = "input_addon"><i class="fa fa-lock"></i></span>
                  <input type="text" class="form-control" name="password" placeholder="Password">
                </div>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="check-box"> <span>
                <input id="pre2" name="pre2" class="pre2" type="checkbox">
                <label for="pre2">I'd like to receive notification and offers.</label>
                </span> </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <button type="submit" class="btn_submit">Sign Up</button>
              </div>
            </div>
          </div>
     
          <div class="term_c">
            <p>By signing up, you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy.</a></p>
          </div>
          <div class="sign_up">
            <p>Already have an account? <a href="#">Log In</a></p>
          </div>
        </div>
      </div>
	  </form>
    </div>
  </div>
</div>
@endsection
