@extends('admin.header-before-login')

@section('content')

<!-- /.login-logo -->
<div class="login-box-body">

    <!-- for validation errors -->
    @if(count($errors) > 0)
    <div id="error" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        @foreach($errors->all() as $error)
        <div class="msg">{{$error}}</div>
        @endforeach
    </div>
    @endif



    @if(Session::get('error_msg'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        {{Session::get('error_msg')}}
    </div>
    @elseif(Session::get('success_msg'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Success !</h4>
        {{Session::get('success_msg')}}
    </div>
    @endif



    <p class="login-box-msg">Sign In</p>

    <form action="login" method="post" action="{{ route('adminLogin') }}">
        {!! csrf_field() !!}
        <input type="hidden" name="role_type" value="{{config('constants.ROLE_TYPE.admin')}}" >
        <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="timezone" value="">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <br/>
    <p><a href="forgot-password">I forgot my password</a></p>


</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('AdminLTE/js/moment.js')}}"></script>
<script src="{{asset('AdminLTE/js/moment.timezone.js')}}"></script>
<script>
    var timezone = moment.tz.guess();
    $('input[name="timezone"]').val(timezone);
    $(document).ready(function(){
            if ($('.alert-success').length > 0) {
                setInterval(function () {
                    $('.alert-success').fadeOut("slow");
                }, 3000);
            }
            if ($('.alert-danger').length > 0) {
                setInterval(function () {
                    $('.alert-danger').fadeOut("slow");
                }, 3000);
            } 
    });
</script>
</body>
</html>
@stop
