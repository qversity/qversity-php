{!! Form::open(['route' => ['role.destroy', $encrypted_id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('role.permissions', $encrypted_id) }}">View Permissions</a>
</div>
{!! Form::close() !!}
