
<table id="role" class="table table-condensed">
    <thead>
        <tr>
            <th>Role</th>
            <th>Description</th> 
            <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
  
    <script>
        $(function () {
            $('#role').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                autoWidth: false,
                ajax: '{{ url("admin/role") }}',
                order: [[ 0, "desc" ]],
                aoColumns: [
                    {"data":"name","name":"name"},
                    {"data":"description","name":"description"},
                    {"data":"action","name":"action", seachable: false, orderable: false}
                ]
            });
        });
    </script>
@endsection