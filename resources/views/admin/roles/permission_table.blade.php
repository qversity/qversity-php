
<table id="role" class="table table-condensed">
    <thead>
        <tr>
            <th>Permission</th>
            <th>Description</th> 
            <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
  
    <script>
        $(function () {
            $('#role').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                autoWidth: false,
                ajax: '{{ route('role.permissions',$role->encryptedId) }}',
                order: [[ 0, "desc" ]],
                aoColumns: [
                    {"data":"permission.name","name":"permission.name"},
                    {"data":"permission.description","name":"permission.description"},
                    {"data":"action","name":"action", searchable: false, orderable: false}
                ]
            });
        });
    </script>
@endsection