@extends('admin.layout.main')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Permissions</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right"  style="margin-top: -10px;margin-bottom: 5px" href="{!! route('role.create') !!}">Add Permission</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                  
                
        @foreach ($data as $row) 
           
        <p>{{ $row->permission->id }}</p>
        <p>{{ $row->permission->name }}</p>
         
           
        @endforeach
                
               
            </div>
        </div>
    </div>
@endsection