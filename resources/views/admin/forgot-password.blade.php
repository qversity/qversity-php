@extends('admin.header-before-login')

@section('content')

 <!-- for validation errors -->
    @if(count($errors) > 0)
      <div id="error" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        @foreach($errors->all() as $error)
          <div class="msg">{{$error}}</div>
        @endforeach
       </div>
    @endif

    @if(Session::get('error_msg'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            {{Session::get('error_msg')}}
        </div>
    @endif

    @if(Session::get('success_msg'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {{Session::get('success_msg')}}
        </div>
    @endif
  <div class="login-box-body">
    <p class="login-box-msg">Enter your Email Id</p>

    <form action="forgot-password" method="post">
      <input type="hidden" name="role_type" value="{{config('constants.ROLE_TYPE.admin')}}">
      <div class="form-group has-feedback">
        <input type="email" name="email"class="form-control" placeholder="Email" value="" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Continue</button>
        </div>
        <div class="col-xs-4">
          <a href="javascript:history.go(-1);" class="btn btn-default">Cancel</a>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
            if ($('.alert-success').length > 0) {
                setInterval(function () {
                    $('.alert-success').fadeOut("slow");
                }, 3000);
            }
            if ($('.alert-danger').length > 0) {
                setInterval(function () {
                    $('.alert-danger').fadeOut("slow");
                }, 3000);
            } 
    });
</script>
</body>
</html>
@stop
