<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $users->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $users->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $users->username !!}</p>
</div>

<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>
        @if($users->is_active == \App\Models\User::STATUS_ACTIVE)
            Active
        @else
            Inactive
        @endif    
    </p>
</div>
