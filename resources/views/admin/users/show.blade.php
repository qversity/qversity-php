@extends('admin.layout.main')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Users</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" target="_blank" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('admin/users/export-post/'.$users->id) !!}">Export Related Post</a>
        </h1>
    </section>
<div class="content" style="margin-top: 20px">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.users.show_fields')
                    <a href="{!! url('admin/users') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
