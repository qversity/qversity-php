{!! Form::open(['route' => ['user.destroy', $encrypted_id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ url('admin/user', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('user.edit', $encrypted_id) }}" class='btn btn-info btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>

    @if($status == \App\Models\User::STATUS_ACTIVE)
        <a href="{{ route('admin.user.toggleStatus', $encrypted_id) }}" class='btn btn-success btn-xs'>
            <i class="glyphicon glyphicon-ok-circle"></i>
        </a>
    @else
        <a href="{{ route('admin.user.toggleStatus', $encrypted_id) }}" class='btn btn-danger btn-xs'>
            <i class="glyphicon glyphicon-ban-circle"></i>
        </a>

    @endif
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Are you sure that you want to delete user?')"
    ]) !!}
</div>
{!! Form::close() !!}
