

<table id="users" class="table table-condensed">
    <thead>
        <tr>
            <th>Email</th>
            <th>Name</th>
            <th>Role</th>
            <th>Status</th>  
            <th>Action</th>
        </tr>
    </thead>
</table>

@section('scripts')
  
    <script>
        $(function () {
            $('#users').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                autoWidth: false,
                ajax: '{{ url("admin/user") }}',
                order: [[ 0, "desc" ]],
                aoColumns: [
                    {"data":"email","name":"email"},
                    {"data":"name","name":"name"},
                    {"data":"role_name","name":"role_name"},
                    {"data":"status","name":"status"},
                    {"data":"action","name":"action", seachable: false, orderable: false}
                ]
            });
        });
    </script>
@endsection