<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', 'Role:') !!}
    {!! Form::select('role', array('Select Role','Super Admin','Admin','Primary University','Secondary University','Instructor','Student'),null, ['class' => 'form-control select2']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('admin/user') !!}" class="btn btn-default">Cancel</a>
</div>


@section('scripts')
<script>
    $(function () {
         $('.select2').select2();

         $AUTOCOMPLETE_JS$


    });
</script>
@endsection