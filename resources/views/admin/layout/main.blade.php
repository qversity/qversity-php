<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rcade | Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('adminLTE/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{asset('adminLTE/plugins/select2/select2.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('adminLTE/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminLTE/dist/css/adminLTE.css')}}">
    <link rel="stylesheet" href="{{asset('adminLTE/dist/css/custom.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- adminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{asset('adminLTE/dist/css/skins/skin-blue.min.css')}}">

    <link rel="stylesheet" href="{{asset('adminLTE/plugins/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminLTE/dist/css/bootstrap-tokenfield.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{asset('adminLTE/dist/css/jquery.timepicker.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('adminLTE/dist/css/bootstrap-datepicker.css')}}" />

    <!-- jQuery 2.1.4 -->
    <script src="{{asset('adminLTE/plugins/jQuery/jQuery-2.2.3.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('adminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminLTE/plugins/fastclick/fastclick.js')}}"></script>
    <script type="text/javascript" src="{{asset('adminLTE/dist/js/jquery.timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('adminLTE/dist/js/bootstrap-datepicker.js')}}"></script>

    <!-- DataTables -->
    <script src="{{asset('adminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        var SITE_URL = "{{url('')}}";
    </script>
    @yield('styles')

    @yield('plugins')
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

     @include('admin.layout.header')

     @include('admin.layout.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

            @yield('content')

      </div><!-- /.content-wrapper -->

      @include('admin.layout.footer')

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>

      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->


    <!-- adminLTE App -->
    <script src="{{asset('adminLTE/dist/js/app.min.js')}}"></script>
    <script src="{{asset('adminLTE/dist/js/text-input-validation.js')}}"></script>
    <script src="{{asset('adminLTE/plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('adminLTE/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>

    <script src="{{asset('adminLTE/plugins/token-input/bootstrap-tokenfield.js')}}"></script>


    <script src="{{asset('adminLTE/js/moment.js')}}"></script>
    <script src="{{asset('adminLTE/js/moment.timezone.js')}}"></script>
    <script src="{{asset('adminLTE/js/common.js')}}"></script>

   
    
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
        @yield('modal')
      @yield('scripts')
  </body>
</html>
