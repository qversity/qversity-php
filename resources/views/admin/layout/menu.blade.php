
<li class="{{ Request::is('admin/user/*') || Request::is('admin/user') ? 'active' : '' }}">
    <a href="{!! url('admin/user') !!}"><i class="fa fa-user"></i><span>Users</span></a>
</li>
<li class="{{ Request::is('admin/role/*') || Request::is('admin/role') ? 'active' : '' }}">
    <a href="{!! url('admin/role') !!}"><i class="fa fa-users"></i><span>Roles</span></a>
</li>
<li class="{{ Request::is('admin/course/*') || Request::is('admin/course') ? 'active' : '' }}">
    <a href="{!! url('admin/course') !!}"><i class="fa fa-book"></i><span>Course</span></a>
</li>



