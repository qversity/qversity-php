<?php $role_type = Auth::user()->role_type; ?>
<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
           <!--  <li class="header">HEADER</li> -->
            <!-- Optionally, you can add icons to the links -->

            <li>
               <a href="{{url('admin/')}}">
               <i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            @include('admin.layout.menu')

          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
