 <!-- Main Header -->
      <header class="main-header">
          
        <!-- Logo -->
          <a href="{{url('/admin/dashboard')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <!-- <span class="logo-mini"><b>A</b>LT</span> -->
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b> Panel</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="{{asset('images/default-user.jpeg')}}" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">{{ucfirst(Auth::user()->name)}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="{{asset('images/default-user.jpeg')}}" class="img-circle" alt="User Image">
                    <p>
                      {{ucfirst(Auth::user()->name)}}
                    </p>
                  </li>

                  <li class="user" style="padding:7px 0;font-size:16px;">
                    <div class="row">
                      <div class="col-xs-12 text-center">
                        <a href="{{url('admin/change-password')}}" >Change Password</a>
                      </div>
                    </div>
                    <!-- /.row -->
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="settings" class="btn btn-default btn-flat hide">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{route('admin.logout')}}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             <!--  <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
            </ul>
          </div>
        </nav>
      </header>
