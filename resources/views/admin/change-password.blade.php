@extends('admin.layout.main')

@section('content')
@section('scripts')
<script>

 $(document).on('submit','#change-password',function(e){
//    return passwordConfirmationMatches();
 });


 $(document).on('click','#alertModal .ok',function(e){
         $('#alertModal').modal('hide');
     });

  function passwordConfirmationMatches()
 {
    if($('#password').val() !==  $('#password_confirmation').val() )
    {
      var msg = "password and confirm password fields do not match";
      $('#alertModal').find('.msg').text(msg);
      $('#alertModal').modal('show');
      return false;
    }

    return true;
 }


 </script>
@stop

  <!-- Main content -->
  <section class="content">

  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Change Password</h3>
      </div><!-- /.box-header -->


      <div class="box-body">

        <!-- for validation errors -->
        @if(count($errors) > 0)
          <div id="error" class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            @foreach($errors->all() as $error)
              <div class="msg">{{$error}}</div>
            @endforeach
           </div>
        @endif


        @if(Session::get('error_msg'))
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              {{Session::get('error_msg')}}
            </div>
          @elseif(Session::get('success_msg'))
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h4><i class="icon fa fa-check"></i> Success !</h4>
              {{Session::get('success_msg')}}
            </div>
        @endif

        <form role="form" id="change-password" method="post" action="change-password">

            <div class="form-group">
              <label>Old Password</label>
              <input name="old_password" minlength="6" id="old_password" maxlength="20" required  type="password" value="" class="form-control" placeholder="Old Password">
            </div>


            <div class="form-group">
              <label>New Password</label>
              <input name="password" minlength="6" id="password" maxlength="20" required  type="password" value="" class="form-control" placeholder="New Password">
            </div>

            <div class="form-group">
              <label>Confirm Password</label>
              <input name="password_confirmation" id="password_confirmation"  minlength="6" maxlength="20" required  type="password" value="" class="form-control" placeholder="Confirm Password">
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <button type="submit" class="btn btn-primary">Change</button>
        </form>

      </div><!-- /.box-body -->


    </div>

    @include('admin.layout.alert-box')

  </section><!-- /.content -->
@stop
