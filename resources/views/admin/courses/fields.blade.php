<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Course Title:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Course Category:') !!}
    {!! Form::select('category', array('Select Category','Business','Finance','Photography','Technology'),null, ['class' => 'form-control select2']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('admin/user') !!}" class="btn btn-default">Cancel</a>
</div>


@section('scripts')
<script>
    $(function () {
         $('.select2').select2();

         $AUTOCOMPLETE_JS$


    });
</script>
@endsection