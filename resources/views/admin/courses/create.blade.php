@extends('admin.layout.main')

@section('content')
    <section class="content-header">
        <h1>
            Courses
        </h1>
    </section>
    <div class="content">
        @include('admin.layout.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'course.store']) !!}

                        @include('admin.courses.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection



