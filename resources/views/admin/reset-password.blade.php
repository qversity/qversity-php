@extends('admin.header-before-login')

@section('content')

  <div class="login-box-body">

     <!-- for validation errors -->
    @if(count($errors) > 0)
      <div id="error" class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        @foreach($errors->all() as $error)
          <div class="msg">{{$error}}</div>
        @endforeach
       </div>
    @endif


    @if(Session::get('error_msg'))
      <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        {{Session::get('error_msg')}}
      </div>
    @endif

    <p class="login-box-msg">Reset Password</p>

    <form action="reset-password?token={{$token}}" method="post">
      <input class="form-control" type="hidden"  name="id" value="{{$user_id}}" placeholder="" required>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="timezone" value="">
      <div class="form-group has-feedback">
        <input type="password" id="password" name="password" minlength=6 maxlength=20 class="form-control" placeholder="New Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="confirm_password" name="password_confirmation" minlength=6 maxlength=20 class="form-control" placeholder="Confirm Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.login-box-body -->


</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('AdminLTE/js/moment.js')}}"></script>
<script src="{{asset('AdminLTE/js/moment.timezone.js')}}"></script>
<script>
    var timezone = moment.tz.guess();
    $('input[name="timezone"]').val(timezone);
    $(document).ready(function(){
            if ($('.alert-success').length > 0) {
                setInterval(function () {
                    $('.alert-success').fadeOut("slow");
                }, 3000);
            }
            if ($('.alert-danger').length > 0) {
                setInterval(function () {
                    $('.alert-danger').fadeOut("slow");
                }, 3000);
            } 
    });
</script>
<script src="{{asset('admin/js/reset-password.js')}}"></script>
</body>
</html>
@stop
