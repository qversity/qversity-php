@extends('admin.layout.main')

@section('content')

<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Dashboard</h3>
        </div>


        <div class="box-body">
            <div class="col-md-12 m-t-20 box-aligned">


                    <div class="col-lg-3 col-xs-6">

                        <div class="small-box bg-aqua">
                            <div class="panel-heading ">
                                <div class="panel-title">TOTAL STUDENTS</div>
                            </div>
                            <div class="inner">
                                <h3>2847562</h3>
                                <p>Total students Registered on Qversity</p>
                            </div>
                            <a href="{{route('user.index')}}" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-xs-6">

                        <div class="small-box bg-green">
                            <div class="panel-heading ">
                                <div class="panel-title">TOTAL INSTRUCTOR</div>
                            </div>
                            <div class="inner">
                                <h3>10</h3>

                                <p>Total instructor registered on Qversity</p>
                            </div>

                            <a href="{{route('user.index')}}" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-yellow">
                            <div class="panel-heading ">
                                <div class="panel-title">TOTAL COURSES</div>
                            </div>
                            <div class="inner">
                                <h3>10</h3>

                                <p>Total available courses</p>
                            </div>

                            <a href="javascript:void(0);" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-red">
                            <div class="panel-heading ">
                                <div class="panel-title">PRIMARY UNIVERSITY</div>
                            </div>
                            <div class="inner">
                                <h3>10</h3>

                                <p>Universities affliating courses</p>
                            </div>

                            <a href="javascript:void(0);" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-light-blue">
                            <div class="panel-heading ">
                                <div class="panel-title">SECONDARY UNIVERSITY</div>
                            </div>
                            <div class="inner">
                                <h3>10</h3>
                                <p>Local Universities </p>
                            </div>
                            <a href="javascript:void(0);" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-olive">
                            <div class="panel-heading ">
                                    <div class="panel-title">TOTAL BATCH</div>
                            </div>
                            <div class="inner">
                                <h3>10</h3>

                                <p>Total batches running currently</p>
                            </div>

                            <a href="javascript:void(0);" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>


                    {{--<div class="col-lg-3 col-xs-6">--}}
                        {{--<div class="small-box bg-teal">--}}
                            {{--<div class="panel-heading ">--}}
                                {{--<div class="panel-title">TOTAL ADDITIONAL CHARGES</div>--}}
                            {{--</div>--}}
                            {{--<div class="inner">--}}
                                {{--<h3><sup style="font-size: 20px">&#36;</sup>0</h3>--}}

                                {{--<p>Additional amount charged by Ops manager</p>--}}
                            {{--</div>--}}

                            {{--<a href="javascript:void(0);" class="small-box-footer">--}}
                                {{--More info <i class="fa fa-arrow-circle-right"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    {{--<div class="col-lg-3 col-xs-6">--}}
                        {{--<div class="small-box bg-teal">--}}
                            {{--<div class="panel-heading ">--}}
                                {{--<div class="panel-title">TOTAL ADDITIONAL CHARGES</div>--}}
                            {{--</div>--}}
                            {{--<div class="inner">--}}
                                {{--<h3><sup style="font-size: 20px">&#36;</sup>0</h3>--}}

                                {{--<p>Additional amount charged by Admin</p>--}}
                            {{--</div>--}}

                            {{--<a href="javascript:void(0);" class="small-box-footer">--}}
                                {{--More info <i class="fa fa-arrow-circle-right"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    {{--<div class="col-lg-3 col-xs-6">--}}
                        {{--<div class="small-box bg-aqua">--}}
                            {{--<div class="panel-heading ">--}}
                                {{--<div class="panel-title">TOTAL JOBS</div>--}}
                            {{--</div>--}}
                            {{--<div class="inner">--}}
                                {{--<h3>10</h3>--}}
                                {{--<p>BY OPERATION MANAGER</p>--}}
                            {{--</div>--}}
                            {{--<a href="javascript:void(0);" class="small-box-footer">--}}
                                {{--More info <i class="fa fa-arrow-circle-right"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
            </div>



        </div>
    </div>
</section>
@stop


