<header>
  <div class="header_top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-7">
          <div class="phone_no">
            <p>Have any question? +1 929.841.5969</p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-5">
          <div class="social_media"> <span>Follow Us</span>
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="menu_bar">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-3 col-md-2">
          <a href="index.html" class="logo"> <img src="images/logo.png" class="img-responsive" alt="Logo"> </a>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-5">
          <div class="title_head"><a href="#"><img src="images/icon.png"><span>Browse Courses</span></a>
            <div class="course_list" style="display:none;">
              <ul>
                <li><a href="financial_modeling.html">Financial Modeling</a></li>
                <li><a href="#">Busines Leader's Guide</a></li>
                <li><a href="#">Computer Science</a></li>
                <li><a href="#">Micro Biology</a></li>
                <li><a href="#">Cyber Security</a></li>
              </ul>
            </div>
          </div>
          <div class="title_box">
            <div class = "input-group">
              <input type = "text" class = "form-control" placeholder="Search Courses">
              <span class = "input-group-btn">
              <a href="specializations_courses.html" class = "btn btn-default" type = "button"> <i class="fa fa-search"></i> </a>
              </span> </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-5">
          <div class="sm_nav"><i class="fa fa-bars"></i></div>
          <div class="login_sec">
            <ul>
              <li><a href="#">Teach for Qversity</a></li>
              <li><a data-toggle = "modal" data-target = "#login">Log in</a></li>
              <li><a data-toggle = "modal" data-target = "#signup" class="signup" href="#">Sign up</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>