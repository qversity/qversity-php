<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>QVersity ::</title>
<link href="{{ URL::to('css/font-awesome.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::to('css/bootstrap.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::to('css/style.css') }}" type="text/css" rel="stylesheet">
<link href="{{ URL::to('css/style.css') }}" type="text/css" rel="stylesheet">
</head>
<body>  
@include('includes.header')  
@yield('content')
<script type="text/javascript" src="{{ URL::to('js/jquery-1.11.0.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::to('js/bootstrap.js') }}"></script> 
<script type="text/javascript" src="{{ URL::to('js/owl.carousel.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::to('js/custom.js') }}"></script>
</body>
</html>
