<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['adminAuth']], function () {
	Route::get('/','AdminController@dashboard')->name('admin.dashboard');
	Route::get('/user/toggle-status/{id}','Admin\UserController@toggleStatus')->name('admin.user.toggleStatus');
	Route::resource('user','Admin\UserController');
	Route::get('logout','AdminController@logout')->name('admin.logout');
        Route::resource('role','Admin\RoleController');
        Route::resource('course','Admin\CourseController');
        Route::get('role/{id}/permissions','Admin\RoleController@rolePermissions')->name('role.permissions');


});

Route::get('/login','AdminController@loginView')->name('admin.login');
Route::post('login','AdminController@login')->name('admin.login.post');

