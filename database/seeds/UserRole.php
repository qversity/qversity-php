<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class UserRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array(
                "id" => 1,
                "name" => "SUPER_ADMIN",
                "description" => "NA",
            ),
            array(
                "id" => 2,
                "name" => "ADMIN",
                "description" => "NA",
            ),
            array(
                "id" => 3,
                "name" => "PRIMARY_UNIVERSITY",
                "description" => "NA",
            ),
            array(
                "id" => 4,
                "name" => "SECONDARY_UNIVERSITY",
                "description" => "NA",
            ),
            array(
                "id" => 5,
                "name" => "INSTRUCTOR",
                "description" => "NA",
            ),
            array(
                "id" => 6,
                "name" => "STUDENT",
                "description" => "NA",
            ),
        );

        Role::truncate();
        Role::insert($roles);
    }
}
