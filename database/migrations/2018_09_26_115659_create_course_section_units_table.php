<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseSectionUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_section_units', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('section_id')->unsigned();
			$table->foreign('section_id')->references('id')->on('course_sections');
			$table->tinyInteger('type');
			$table->string('title',100);
			$table->integer('category')->unsigned();
			$table->foreign('category')->references('id')->on('category');
			$table->text('content');
			$table->text('description');
			$table->tinyInteger('is_free');
			$table->tinyInteger('duration');
			$table->dateTime('access_date_time');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_section_units');
    }
}
