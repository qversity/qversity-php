<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('course_id')->unsigned();
			$table->foreign('course_id')->references('id')->on('courses');
			$table->string('title');
			$table->integer('category')->unsigned();
			$table->foreign('category')->references('id')->on('category');
			$table->text('content');
			$table->string('sub_title',100);
			$table->tinyInteger('duration');
			$table->tinyInteger('auto_evaluate_result');
			$table->tinyInteger('number_of_extra_retake');
			$table->text('post_quiz_msg');
			$table->tinyInteger('show_result_after_submission');
			$table->tinyInteger('auto_chreck_answer_switch');
			$table->tinyInteger('randomize_question');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz');
    }
}
