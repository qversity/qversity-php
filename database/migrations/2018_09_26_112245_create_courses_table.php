<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
			$table->string('course_title',100);
			$table->integer('created_by')->unsigned();
			$table->foreign('created_by')->references('id')->on('users');
			$table->string('category');
			$table->string('short_description',250);
			$table->string('long_description',250);
			$table->char('image',50);
			$table->smallInteger('max_duration');
			$table->integer('prerequisite_course');
			$table->tinyInteger('complete_previous_unit_first');
			$table->tinyInteger('course_type');
			$table->tinyInteger('course_evaluation_mode');
			$table->tinyInteger('certificate_enabled');
			$table->dateTime('start_date');
			$table->text('instructions');
			$table->text('completion_message');
			$table->tinyInteger('max_enrolled_student');
			$table->tinyInteger('is_free');
			$table->decimal('price',10,2);
			$table->decimal('sale_price',10,2);
			$table->tinyInteger('subscription_type');
			$table->tinyInteger('subscription_duration');
			$table->tinyInteger('subscription_duration_type');
			$table->tinyInteger('is_first_section_free');
			$table->tinyInteger('invite_student_application');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
