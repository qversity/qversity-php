<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title',100);
			$table->string('content',45);
			$table->string('sub_title',45);
			$table->string('max_marks',45);
			$table->string('max_time',45);
			$table->tinyInteger('included_in_course_evaluation');
			$table->tinyInteger('submission_type');
			$table->tinyInteger('attachment_type');
			$table->tinyInteger('max_attachment_size');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
