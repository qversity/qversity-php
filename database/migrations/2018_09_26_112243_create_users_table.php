<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',100)->unique();
            $table->string('password')->nullable();
			$table->string('name');
			$table->integer('role')->unsigned();
			$table->foreign('role')->references('id')->on('roles');
			$table->tinyInteger('status');
			$table->dateTime('last_login');
			$table->string('linkedin_id',45)->nullable();
			$table->string('facebook_id',45)->nullable();
			$table->string('googleplus_id',45)->nullable();
            $table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
