<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseForumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_forum', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('course_id')->unsigned();
			$table->integer('section_id')->unsigned();
			$table->integer('forum_id')->unsigned();
			$table->foreign('course_id')->references('id')->on('courses');
			$table->foreign('section_id')->references('id')->on('course_sections');
			$table->foreign('forum_id')->references('id')->on('forum');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_forum');
    }
}
