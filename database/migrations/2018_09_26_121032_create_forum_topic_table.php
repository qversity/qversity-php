<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_topic', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('forum_id')->unsigned();
			$table->foreign('forum_id')->references('id')->on('forum');
			$table->string('title',100);
			$table->string('text',100);
			$table->string('tags',100);
			$table->tinyInteger('type');
			$table->tinyInteger('status');
			$table->tinyInteger('notify_for_reply');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_topic');
    }
}
