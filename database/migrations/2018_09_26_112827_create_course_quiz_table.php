<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_quiz', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('quiz_id')->unsigned();
			$table->foreign('quiz_id')->references('id')->on('quiz');
			$table->integer('course_id')->unsigned();
			$table->foreign('course_id')->references('id')->on('courses');
			$table->tinyInteger('placed_id');
			$table->integer('section_id')->unsigned();
			$table->foreign('section_id')->references('id')->on('course_sections');
			$table->rememberToken();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_quiz');
    }
}
