// CustomJS //



$(document).ready(function() {
	 				   
	//--- Lecturers ---// 
     $("#testimonial-slider").owlCarousel({
        items:3,
        itemsDesktop:[1000,2],
        itemsDesktopSmall:[990,1],
        itemsTablet:[768,2],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        slideSpeed:500,
		  autoplay: 300,
        autoPlay:true
    });
	
	
	//--- Blog ---// 
     $("#testimonial-slider1").owlCarousel({
        items:2,
        itemsDesktop:[1000,2],
        itemsDesktopSmall:[990,1],
        itemsTablet:[768,2],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        slideSpeed:500,
		 autoplay: 300,
        autoPlay:true
    });
	
	//--- Header Browse Course ---//
	$(".title_head").click(function(){
	$(".course_list").slideToggle("slow");
	}); 

	//--- Mobile Nav  ---//
	$(".sm_nav").click(function(){
	$(".login_sec").slideToggle("slow");
	}); 

	
	//--- Talk Form---//
	$(".talk_btn").click(function(){
	$(".talk_form").toggleClass("js-dropdown");
	});
	
	$("#close_b").click(function(){
	$(".talk_form").removeClass("js-dropdown");
	});

    
 
});
	