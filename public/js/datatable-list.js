$(function () {
    var url = SITE_URL + '/admin/games/grid';
    if($('input[name="genre"]').val() != '') {
        url = SITE_URL + '/admin/games/grid/'+$('input[name="genre"]').val();
    }
    $('#games').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth: false,
        ajax: url,
        order: [[0, "asc"]],
        aoColumns: [
            {data: "name", name: "name"},
            {
                data: "image",
                render: function (data, type, row) {
                    if(data) {
                        return '<img src="'+data+'" width="75">';
                    }
                    return 'N/A';
                },
                name: "image",
                orderable: false, searchable: false},
            {data: "game_users_follower_count", name: "game_users_follower_count", searchable: false},
            {data: "game_user_posts_count", name: "game_user_posts_count", searchable: false},
            {data: "action", name: "action", orderable: false}
        ]
    });
});